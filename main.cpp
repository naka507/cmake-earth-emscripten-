#include "Application.hpp"

int main(int argc, const char* argv[]) {
  Application app;
  app.run();
  return 0;
}
