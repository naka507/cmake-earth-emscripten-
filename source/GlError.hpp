

#ifndef GLERROR_HPP
#define GLERROR_HPP

void glCheckError(const char *file, unsigned int line);

#endif
