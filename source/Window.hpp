

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <string>

const unsigned int SCREEN_WIDTH = 800;
const unsigned int SCREEN_HEIGHT = 600;

struct GLFWwindow;
enum State
{
    stateReady,
    stateRun,
    stateExit
};
class Window
{
public:
    Window();

    static Window &getInstance();

    // get the window id
    GLFWwindow *getWindow() const;
    int getWidth();
    int getHeight();

    // window control
    void run();
    void exit();

private:
    State state;

    Window &operator=(const Window &) { return *this; }

    GLFWwindow *window;

    float deltaTime;

    int width;
    int height;
    bool changed;
    void change();

protected:
    Window(const Window &){};

    virtual void loop(float dt) = 0;
    virtual void input(float dt) = 0;
};

#endif
