#ifndef Application_HPP
#define Application_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Window.hpp"

class Application : public Window
{
public:
    Application();
    ~Application();
  
protected:
    virtual void loop(float dt);
    virtual void input(float dt);
};

#endif