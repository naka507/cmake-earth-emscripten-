#include "Window.hpp"
#include "Resource.hpp"
#include "GlError.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <functional>
#include <iostream>
#include <stdexcept>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#include <cmath>
#endif

std::function<void()> registered_loop;
void loop_iteration()
{
    registered_loop();
}

Window *currentWindow = NULL;

Window &Window::getInstance()
{
    if (currentWindow)
        return *currentWindow;
    else
        throw std::runtime_error("There is no current Window");
}

Window::Window()
    : state(stateReady), width(SCREEN_WIDTH), height(SCREEN_HEIGHT)
{
    currentWindow = this;

    std::cout << "[Info] GLFW initialisation" << std::endl;

    // initialize the GLFW library
    if (!glfwInit())
    {
        throw std::runtime_error("Couldn't init GLFW");
    }

    // setting the opengl version
#ifdef __EMSCRIPTEN__
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
#else
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    window = glfwCreateWindow(width, height, "Window", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        throw std::runtime_error("Couldn't create a window");
    }

    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (err != GLEW_OK)
    {
        glfwTerminate();
        throw std::runtime_error(std::string("Could initialize GLEW, error = ") +
                                 (const char *)glewGetErrorString(err));
    }

    const GLubyte *renderer = glGetString(GL_RENDERER);
    const GLubyte *version = glGetString(GL_VERSION);
    std::cout << "Renderer: " << renderer << std::endl;
    std::cout << "OpenGL version supported " << version << std::endl;

    glViewport(0, 0, width, height);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

GLFWwindow *Window::getWindow() const
{
    return window;
}

int Window::getWidth()
{
    return width;
}

int Window::getHeight()
{
    return height;
}

void Window::exit()
{
    state = stateExit;
}

void Window::run()
{
    state = stateRun;

    glfwMakeContextCurrent(window);

    float time = 0.0f;
    float frame = 0.0f;

    registered_loop = [&]()
    {
        float current = static_cast<float>(glfwGetTime());
        time = current - frame;
        frame = current;

        glfwPollEvents();

        input(time);

        change();

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        loop(time);

        glfwSwapBuffers(window);

        // glCheckError(__FILE__, __LINE__);
    };

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(loop_iteration, 0, 1);
#else
    while (state == stateRun)
    {
        loop_iteration();
    }
    glfwTerminate();
#endif
}

void Window::change()
{
    int w, h;
#ifdef __EMSCRIPTEN__
    emscripten_get_canvas_element_size("canvas", &w, &h);
#else
    glfwGetWindowSize(getWindow(), &w, &h);
#endif
    changed = (w != width || h != height);
    width = w;
    height = h;
    glViewport(0, 0, width, height);
}