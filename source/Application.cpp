#include "Application.hpp"
#include "Resource.hpp"

#include <functional>
#include <iostream>
#include <stdexcept>



Application::Application()
{

}

Application::~Application()
{
}

void Application::input(float deltaTime)
{
    if (glfwGetKey(getWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        std::cout << "[Info] INPUT GLFW_PRESS" << std::endl;
        glfwSetWindowShouldClose(getWindow(), true);
    }
}

void Application::loop(float deltaTime)
{
    if (glfwWindowShouldClose(getWindow()))
        exit();
}